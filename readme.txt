Seen is a small module which tracks which users have viewed a particular node
(based on node access records), and provides this information as a tab on each
node.

Credits

This module was written by Christopher Skene.

PreviousNext can also be contacted for paid customizations of this module as
well as Drupal consulting, installation and development.

Development was sponsored by:

- The Australian Defence Force Academy, through their Cadet Intranet project